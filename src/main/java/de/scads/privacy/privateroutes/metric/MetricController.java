package de.scads.privacy.privateroutes.metric;


import de.scads.privacy.privateroutes.dto.bidirectional.MetricMetadataDto;
import de.scads.privacy.privateroutes.privacyService.PrivacyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping("api/metric")
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class MetricController {

    private final PrivacyService privacyService;

    @Autowired
    public MetricController(
            PrivacyService privacyService
    ) {
        this.privacyService = privacyService;
    }

    @GetMapping("")
    public MetricMetadataDto[] getAvailableMetrics(){
        return privacyService.getAvailableMetrics();
    }

}
