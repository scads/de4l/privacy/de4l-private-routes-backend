package de.scads.privacy.privateroutes.point;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import de.scads.privacy.privateroutes.measurement.Measurement;
import de.scads.privacy.privateroutes.route.Route;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.geolatte.geom.G2D;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Point")
@Table(name = "privacy_tuna_point")
public class Point implements Serializable {

    public Point(org.geolatte.geom.Point<G2D> g2DPoint,  Date date) {
        this.geography = g2DPoint;
        this.timestamp = date;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(
            name = "id",
            updatable = false
    )
    Long id;

    @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    @Column(
            name = "timestamp",
            nullable = false
    )
    private Date timestamp;

    @ToString.Exclude
    @ManyToOne( fetch = FetchType.LAZY )
    private Route route;

    // Hibernate-Spatial support
    // Points as Geographic Points
    private org.geolatte.geom.Point<G2D> geography;

    @OneToMany(
            mappedBy = "point",
            fetch = FetchType.LAZY,
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    private List<Measurement> measurements;

    @JsonIgnore
    public Double getLatitude(){
        return geography.getPosition().getLat();
    }

    @JsonIgnore
    public Double getLongitude(){
        return geography.getPosition().getLon();
    }

    public void setMeasurements(List<Measurement> measurements) {
        this.measurements = measurements;
        for ( Measurement measurement : this.measurements ){
            measurement.setPoint(this);
        }
    }
}

