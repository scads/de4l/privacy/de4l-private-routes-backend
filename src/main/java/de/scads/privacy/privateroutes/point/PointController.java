package de.scads.privacy.privateroutes.point;

import de.scads.privacy.privateroutes.dto.DtoService;
import de.scads.privacy.privateroutes.dto.output.PointDto;
import org.geolatte.geom.G2D;
import org.geolatte.geom.crs.CoordinateReferenceSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("api/point")
public class PointController {

    private final PointService pointService;

    @Autowired
    CoordinateReferenceSystem<G2D> crs;

    private final DtoService dtoService;


    @Autowired
    public PointController(PointService pointService, DtoService dtoService) {
        this.pointService = pointService;
        this.dtoService = dtoService;

    }

    @DeleteMapping(path = "{id}")
    public void deletePointById(@PathVariable("id") Long id) {
        pointService.deletePointById(id);
    }

    @GetMapping("")
    public List<PointDto> getAllPoints() {
        return pointService.getAllPoints().stream()
                .map(dtoService::convertPoint)
                .collect(Collectors.toList());
    }

    @GetMapping(path = "{id}")
    public PointDto getPointById(@PathVariable("id") Long id) {
    return pointService.getPointById(id)
      .map(dtoService::convertPoint)
      .orElse(null);
    }

}
