package de.scads.privacy.privateroutes.point;

import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.hibernate.annotations.OnDelete;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface PointRepository extends JpaRepository<Point, Long> {

    boolean existsByGeographyAndTimestamp(org.geolatte.geom.Point<G2D> geography, Date timestamp);

    Optional<Point> findByGeographyAndTimestamp(org.geolatte.geom.Point<G2D> geography, Date timestamp);

    @Query(
            value = "Select * FROM Point p WHERE st_within(p.geography, ?1 )",
            nativeQuery = true
    )
    List<Point> findAllWithin(Geometry<G2D> geometry);

    @Modifying
    @Transactional
    @Query( value = "delete from Point where id = :id")
    void deletePointById(Long id);
}
