package de.scads.privacy.privateroutes.point;

import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PointService {

    private final PointRepository pointRepository;

    @Autowired
    public PointService(PointRepository pointRepository) {
        this.pointRepository = pointRepository;
    }

    public List<Point> getAllPoints() {
        return pointRepository.findAll();
    }

    public Optional<Point> getPointById(Long id) {
        return pointRepository.findById(id);
    }

    public void deletePointById(Long id) {
        pointRepository.deletePointById(id);
    }

    public List<Point> getPointsWithin(Geometry<G2D> geometry) {
        return pointRepository.findAllWithin(geometry);
    }
}
