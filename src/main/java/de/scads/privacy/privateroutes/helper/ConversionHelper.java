package de.scads.privacy.privateroutes.helper;

import de.scads.privacy.privateroutes.dto.input.GenericRouteDto;
import de.scads.privacy.privateroutes.point.Point;
import de.scads.privacy.privateroutes.route.Route;
import de.scads.privacy.privateroutes.stop.Stop;
import org.geolatte.geom.*;
import org.geolatte.geom.crs.CoordinateReferenceSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.geolatte.geom.builder.DSL.*;


@Component
public class ConversionHelper {

    @Autowired
    private CoordinateReferenceSystem<G2D> crs;

    public Route convertGenericRouteToRoute(GenericRouteDto genericRouteDto) throws ParseException {
        Route route = new Route();
        route.setName(genericRouteDto.getName());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-DD-mm");
        route.setDate(df.parse(genericRouteDto.getDate()));
        route.setPoints( this.convertGenericRouteDtoToPoints(
                        genericRouteDto.getRoute(),
                        genericRouteDto.getTimestamps()
                )
        );
        route.setStops( this.convertGenericRouteDtoToStops(
                        genericRouteDto.getRoute(),
                        genericRouteDto.getTimestamps(),
                        genericRouteDto.getStops()
                )
        );
        return route;
    }

    // Parses String '2013-07-01 02:04:10.00000' to Date
    public Date parseTimestamp(String input) throws ParseException {
        String datePattern =  "yyyy-MM-dd HH:mm:ss.SSSSSS";
        SimpleDateFormat df = new SimpleDateFormat(datePattern);
        try {
            return df.parse(input);
        } catch (Exception e ) {
            throw new ParseException("Couldn't parse timestamp : \"" + input + "\" to pattern" + datePattern, 0);
        }
    }

    public List<Stop> convertGenericRouteDtoToStops( Double[][] latlngs, String[] timestamps, int[] stopIds ) throws ParseException  {
        List<Stop> stops = new ArrayList<>();
        for ( Integer i : stopIds ){
            if ( i >= latlngs.length) {
                throw new ParseException("Maximum stop id must be smaller than count of coordinates! " +
                        "stop id = " + i + ", coordinates.length = " + latlngs.length,0);
            }
            org.geolatte.geom.Point<G2D> coords = point(crs, g(latlngs[i][0], latlngs[i][1]));
            Date timestamp = this.parseTimestamp(timestamps[i]);
            stops.add(new Stop(coords, timestamp));
        }
        return stops;
    }

    public List<Point> convertGenericRouteDtoToPoints( Double[][] latlngs, String[] timestamps ) throws ParseException  {
        if ( latlngs.length != timestamps.length ){
            throw new ParseException("Count of timestamps has to match the count of coordinates! coordinates.length = " +
                    latlngs.length + ", timestamps.length = " + timestamps.length,0);
        }
        List<Point> points = new ArrayList<>();
        for ( int i = 0; i < latlngs.length; i++ ){
            org.geolatte.geom.Point<G2D> coords = point(crs, g(latlngs[i][0], latlngs[i][1]));
            Date timestamp = this.parseTimestamp(timestamps[i]);
            points.add(new Point(coords, timestamp));
        }
        return points;
    }

}
