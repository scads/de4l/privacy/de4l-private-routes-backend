package de.scads.privacy.privateroutes.dto;

import de.scads.privacy.privateroutes.dto.bidirectional.MeasurementDto;
import de.scads.privacy.privateroutes.dto.output.*;
import de.scads.privacy.privateroutes.measurement.Measurement;
import de.scads.privacy.privateroutes.point.Point;
import de.scads.privacy.privateroutes.route.Route;

public interface DtoService {

  MeasurementDto convertMeasurement(Measurement measurement);

  Measurement convertMeasurementDto(MeasurementDto measurementDto);

  RouteDto convertRoute(Route route);

  PointDto convertPoint(Point point);
}
