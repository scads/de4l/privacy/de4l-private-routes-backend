package de.scads.privacy.privateroutes.dto.output;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class PrivacyServiceRequestDto {
    List<RouteDto> routes;
    List<RouteWithMetricsDto> privateRoutes;
    List<Long> metricIds;
    Boolean calculateNonPrivateMetrics;
    PrivacyAlgorithmSettingsDto privacyAlgorithm;
}
