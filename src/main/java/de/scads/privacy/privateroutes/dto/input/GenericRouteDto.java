package de.scads.privacy.privateroutes.dto.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.scads.privacy.privateroutes.dto.bidirectional.MeasurementDto;
import lombok.Data;


@Data
public class GenericRouteDto {
    @JsonProperty("name")
    private String name;

    @JsonProperty("date")
    private String date;

    @JsonProperty("route")
    private Double[][] route;

    @JsonProperty("stops")
    private int[] stops;

    @JsonProperty("timestamps")
    private  String[] timestamps;

    @JsonProperty("measurements")
    private MeasurementDto[][] measurements;

}
