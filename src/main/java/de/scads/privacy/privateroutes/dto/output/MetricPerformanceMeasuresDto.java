package de.scads.privacy.privateroutes.dto.output;

import lombok.Data;
import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
public class MetricPerformanceMeasuresDto {
    private Double precision;

    private Double recall;

    private Double f_score;
}
