package de.scads.privacy.privateroutes.dto;

import de.scads.privacy.privateroutes.dto.bidirectional.MeasurementDto;
import de.scads.privacy.privateroutes.dto.output.*;
import de.scads.privacy.privateroutes.helper.ConversionHelper;
import de.scads.privacy.privateroutes.measurement.Measurement;
import de.scads.privacy.privateroutes.point.Point;
import de.scads.privacy.privateroutes.route.Route;
import de.scads.privacy.privateroutes.stop.Stop;
import org.geolatte.geom.G2D;
import org.geolatte.geom.crs.CoordinateReferenceSystem;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DtoServiceImpl implements DtoService {

  ConversionHelper conversionHelper;
  private final CoordinateReferenceSystem<G2D> crs;

  private static final ModelMapper modelMapper = new ModelMapper();

  @Autowired
  public DtoServiceImpl(CoordinateReferenceSystem<G2D> crs, ConversionHelper conversionHelper){
    this.crs = crs;
    this.conversionHelper = conversionHelper;
    this.addPointToPointDtoMap();
    this.addStopToStopDtoMap();
  }

  private void addPointToPointDtoMap() {
    TypeMap<Point, PointDto> typeMap = modelMapper.createTypeMap(Point.class, PointDto.class);
    typeMap.addMappings(mapping -> mapping.map(Point::getLongitude, PointDto::setLongitude))
            .addMappings(mapping -> mapping.map(Point::getLatitude, PointDto::setLatitude));
  }

  private void addStopToStopDtoMap() {
    TypeMap<Stop, StopDto> typeMap = modelMapper.createTypeMap(Stop.class, StopDto.class);
    typeMap.addMappings(mapping -> mapping.map(Stop::getLongitude, StopDto::setLongitude))
            .addMappings(mapping -> mapping.map(Stop::getLatitude, StopDto::setLatitude));
  }

  @Override
  public Measurement convertMeasurementDto(MeasurementDto measurementDto) {
    return modelMapper.map(measurementDto, Measurement.class);
  }

  @Override
  public MeasurementDto convertMeasurement(Measurement measurement) {
    return modelMapper.map(measurement, MeasurementDto.class);
  }

  @Override
  public PointDto convertPoint(Point point) {
    return modelMapper.map(point, PointDto.class);
  }

  @Override
  public RouteDto convertRoute(Route route) {
    return modelMapper.map(route, RouteDto.class);
  }

}
