package de.scads.privacy.privateroutes.dto.output;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteWithMetricsDto {
    Long id;

    String name;

    @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss")
    Date date;

    List<PointWithMetricDto> points;

    List<MetricDto> metrics;

    List<StopDto> stops;

    public void addNewMetrics(RouteWithMetricsDto routeWithNewMetrics) {
        for (PointWithMetricDto point : this.points) {
            if (point.metrics == null ) {
                point.setMetrics( new ArrayList<>() );
            }
            PointWithMetricDto pointWithNewMetrics = routeWithNewMetrics.points.stream().filter(p -> Objects.equals(p.id, point.id))
                    .collect(Collectors.toList()).get(0);
            point.setMetrics(Stream.concat(point.metrics.stream(), pointWithNewMetrics.metrics.stream())
                    .collect(Collectors.toList()));
        }
        this.metrics.stream().filter(metric -> routeWithNewMetrics.metrics.stream()
                        .noneMatch(existing -> Objects.equals(existing.getId(), metric.getId())))
                .forEach(routeWithNewMetrics.metrics::add);
        routeWithNewMetrics.metrics.sort(Comparator.comparing(MetricDto::getId));
        this.metrics = routeWithNewMetrics.metrics;
    }
}
