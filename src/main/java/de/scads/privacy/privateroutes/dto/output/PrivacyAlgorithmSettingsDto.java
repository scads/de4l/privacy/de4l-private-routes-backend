package de.scads.privacy.privateroutes.dto.output;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class PrivacyAlgorithmSettingsDto {
    Long id;
    List<Double> parameters;
}
