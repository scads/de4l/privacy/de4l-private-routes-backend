package de.scads.privacy.privateroutes.dto.output;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MetricDto {

    Long id;

    String name;

    Double value;

    // if this is a privacy or a utility metric
    String type;

    MetricPerformanceMeasuresDto performance;
}
