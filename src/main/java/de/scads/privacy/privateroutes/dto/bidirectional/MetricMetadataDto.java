package de.scads.privacy.privateroutes.dto.bidirectional;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MetricMetadataDto {

    /**
     * Die Id der Metrik.
     * Die Id wird verwendet, um die Metrik eindeutig zu identifizieren und
     * wird bei Privatisierungsanfragen mitgegeben.
     */
    private Long id;

    /**
     * Der Name der Metrik
     */
    private String name;

    /**
     * Der Typ der Metrik.
     * Ist es eine Metrik für Utility oder für die Privatisierung?
     */
    private String type;

    /**
     * Beschreibung der Metrik.
     * Was wird berechnet und was kann man an ihr ablesen?
     */
    private String description;

}
