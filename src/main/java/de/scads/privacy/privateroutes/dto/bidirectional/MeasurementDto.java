package de.scads.privacy.privateroutes.dto.bidirectional;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class MeasurementDto {

    @JsonProperty("type")
    private String type;

    @JsonProperty("value")
    private Double value;

}
