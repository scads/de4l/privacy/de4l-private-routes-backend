package de.scads.privacy.privateroutes.dto.bidirectional;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PrivacyAlgorithmParameterDto {

    /**
     * Der Name des Parameters.
     * Zum Beispiel: Alpha
     */
    private String name;

    /**
     * Das Einheitszeichen.
     * Zum Beispiel: m
     */
    private String unit;

    /**
     * Soll die Scala logarithmisch sein oder nicht
     */
    private Boolean logScale;

    /**
     * Der minimale Wert des Parameters.
     * Bei diesem Wert sollte die Privatisierung am schwächsten sein.
     */
    private Double min;

    /**
     * Der maximale Wert des Parameters.
     * Bei diesem Wert soll die Privatisierung am stärksten sein.
     * Sollte so gewählt werden, dass die Laufzeit des Algorithmus nicht zu groß wird.
     */
    private Double max;

    /**
     * Beschreibung des Parameters,
     * sie sollte dem User vermitteln, was mit diesem Parameter eingestellt werden kann.
     */
    private String description;

}
