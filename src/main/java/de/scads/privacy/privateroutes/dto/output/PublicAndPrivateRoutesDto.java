package de.scads.privacy.privateroutes.dto.output;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublicAndPrivateRoutesDto {

    private RoutesAndMetricsDto nonPrivateRoutes;

    private RoutesAndMetricsDto privateRoutes;

    public void addNewMetrics(PublicAndPrivateRoutesDto routesWithNewMetrics) {
        this.nonPrivateRoutes.addNewMetrics(routesWithNewMetrics.getNonPrivateRoutes());
        this.privateRoutes.addNewMetrics(routesWithNewMetrics.getPrivateRoutes());
    }


}
