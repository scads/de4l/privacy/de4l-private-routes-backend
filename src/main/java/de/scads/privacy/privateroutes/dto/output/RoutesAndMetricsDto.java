package de.scads.privacy.privateroutes.dto.output;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoutesAndMetricsDto {

    private List<RouteWithMetricsDto> routes;

    private List<MetricDto> metrics;

    public List<Long> getRouteIds() {
        List<Long> ids = new ArrayList<>();
        for (RouteWithMetricsDto route: routes) {
            ids.add(route.getId());
        }
        return ids;
    }

    public void addNewMetrics(RoutesAndMetricsDto routesWithNewMetrics) {
        for (RouteWithMetricsDto route: this.routes) {
            RouteWithMetricsDto routeWithNewMetrics = routesWithNewMetrics.routes.stream().filter(r -> Objects.equals(r.id, route.id))
                    .collect(Collectors.toList()).get(0);
            route.addNewMetrics(routeWithNewMetrics);
        }
        this.metrics.stream().filter(metric -> routesWithNewMetrics.metrics.stream()
                        .noneMatch(existing -> Objects.equals(existing.getId(), metric.getId())))
                .forEach(routesWithNewMetrics.metrics::add);
        routesWithNewMetrics.metrics.sort(Comparator.comparing(MetricDto::getId));
        this.metrics = routesWithNewMetrics.metrics;
    }

    @JsonIgnore
    public List<Long> getMetricIds() {
        return this.metrics.stream().map(m -> m.id).collect(Collectors.toList());
    }

}
