package de.scads.privacy.privateroutes.dto.bidirectional;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class PrivacyAlgorithmMetadataDto {

    /**
     * Die Id des Algorithmus.
     * Wird bei Privatisierungsanfragen mitgegeben.
     */
    private Long id;

    /**
     * Der Name des Algorithmus
     */
    private String name;

    /**
     * Geordnete Liste der Parameter des Algorithmus.
     * Die Reihenfolge ist wichtig, da bei einer Privatisierungsanfrage,
     * die Parameter in der gleichen Reihenfolge übergeben werden müssen
     */
    private List<PrivacyAlgorithmParameterDto> parameters;

    /**
     * Beschreibung des Algorithmus.
     * Was macht er mit einer Route
     */
    private String description;

}
