package de.scads.privacy.privateroutes.measurement;

import de.scads.privacy.privateroutes.point.Point;
import de.scads.privacy.privateroutes.point.PointRepository;
import de.scads.privacy.privateroutes.route.Route;
import de.scads.privacy.privateroutes.route.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MeasurementService {

    private final MeasurementRepository measurementRepository;

    private final PointRepository pointRepository;

    private final RouteRepository routeRepository;

    @Autowired
    public MeasurementService(MeasurementRepository measurementRepository, PointRepository pointRepository,
                              RouteRepository routeRepository) {
        this.measurementRepository = measurementRepository;
        this.pointRepository = pointRepository;
        this.routeRepository = routeRepository;
    }
/*
    public boolean addMeasurement(Measurement measurement) {
        Point point = measurement.getPoint();
        if (point != null) {
            Route route = point.getRoute();
            if (route != null) {
                // if route present in database, assign it to measurement's point else save it to database
                if (routeRepository.existsByNameAndDate(route.getName(), route.getDate())) {
                    routeRepository.findByNameAndDate(route.getName(), route.getDate())
                            .ifPresent(savedRoute -> measurement.getPoint().setRoute(savedRoute));
                } else {
                    routeRepository.save(route);
                }
            }
            // if point present in database, assign it to measurement else save it to database
            if (pointRepository.existsByGeographyAndTimestamp(point.getGeography(), point.getTimestamp())) {
                pointRepository.findByGeographyAndTimestamp(point.getGeography(), point.getTimestamp())
                        .ifPresent(measurement::setPoint);
            } else {
                if (point.getIsStop() == null) {
                    point.setIsStop(false);
                }
                pointRepository.save(point);
            }
        }
        // save measurement in any case, even if combination of point, type and value is already present in database
        measurementRepository.save(measurement);
        return true;
    }


    public boolean addMeasurements(List<Measurement> measurements) {
        for (Measurement measurement: measurements) {
            addMeasurement(measurement);
        }
        return true;
    }
 */

    public List<Measurement> getAllMeasurements() {
        return measurementRepository.findAll();
    }

    public Optional<Measurement> getMeasurementById(Long id) {
        return measurementRepository.findById(id);
    }

    public void deleteMeasurement(Long id) {
        // todo: check cascading delete of embedded objects
        measurementRepository.deleteById(id);
    }
}
