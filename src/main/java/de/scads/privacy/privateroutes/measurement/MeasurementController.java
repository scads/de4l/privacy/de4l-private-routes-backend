package de.scads.privacy.privateroutes.measurement;

import de.scads.privacy.privateroutes.dto.DtoService;
import de.scads.privacy.privateroutes.dto.bidirectional.MeasurementDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RequestMapping("api/measurement")
@RestController
public class MeasurementController {

    private final MeasurementService measurementService;

    private final DtoService dtoService;

    @Autowired
    public MeasurementController(MeasurementService measurementService, DtoService dtoService) {
        this.measurementService = measurementService;
        this.dtoService = dtoService;
    }

    @GetMapping("")
    public List<MeasurementDto> getAllMeasurements() {
        return measurementService.getAllMeasurements().stream()
                .map(dtoService::convertMeasurement)
                .collect(Collectors.toList());
    }

    @GetMapping(path = "{id}")
    public MeasurementDto getMeasurementById(@PathVariable("id") Long id) {
        return measurementService.getMeasurementById(id)
                .map(dtoService::convertMeasurement)
                .orElse(null);
    }

    @DeleteMapping(path = "{id}")
    public void deleteMeasurementById(@PathVariable("id") Long id) {
        measurementService.deleteMeasurement(id);
    }
}
