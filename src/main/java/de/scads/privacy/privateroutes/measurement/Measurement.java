package de.scads.privacy.privateroutes.measurement;

import de.scads.privacy.privateroutes.point.Point;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "privacy_tuna_measurement")
public class Measurement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(
            name = "id",
            unique = true,
            nullable = false,
            updatable = false
    )
    private Long id;

    @Column(
            name = "type",
            nullable = false
    )
    private String type;

    @Column(
            name = "value",
            nullable = false
    )
    private Double value;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    private Point point;
}
