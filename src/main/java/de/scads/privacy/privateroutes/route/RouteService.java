package de.scads.privacy.privateroutes.route;

import de.scads.privacy.privateroutes.point.Point;
import de.scads.privacy.privateroutes.point.PointRepository;
import de.scads.privacy.privateroutes.point.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RouteService {

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private PointService pointService;

    public boolean addRoute(Route route) {
        if (!routeRepository.existsByNameAndDate(route.getName(), route.getDate())) {
            routeRepository.save(route);
            return true;
        } else {
            return false;
        }
    }

    public boolean addRoutes(List<Route> routes) {
        for (Route route: routes) {
            addRoute(route);
        }
        return true;
    }

    public List<Route> getAllRoutes() {
        return routeRepository.findAll();
    }

    public Optional<Route> getRouteById(Long id) {
        return routeRepository.findById(id);
    }

    public void deleteRoute(Long id) {
        routeRepository.deleteById(id);
    }

    public List<RouteOverview> getOverview(){
        List<RouteOverview> overviews = routeRepository.getOverview();
        for( RouteOverview overview : overviews) {
            overview.setMeasurements( routeRepository.getAvailableMeasurementsByRouteId( overview.id ));
        }
        return overviews;
    }

    public void deleteEveryNthPoint(Long routeId, int everyNth) {
        Optional<Route> opt = getRouteById(routeId);
        if ( opt.isPresent() ) {
            Route route = opt.get();
            Iterator<Point> pointIterator = route.getPoints().iterator();
            int i = 0;
            while (pointIterator.hasNext()) {
                Point p = pointIterator.next();
                if (i % everyNth == 0) {
                    pointIterator.remove();
                    p.setRoute(null);
                    pointService.deletePointById(p.getId());
                }
                i++;
            }
        }
    }
}
