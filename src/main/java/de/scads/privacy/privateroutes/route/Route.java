package de.scads.privacy.privateroutes.route;

import de.scads.privacy.privateroutes.point.Point;
import de.scads.privacy.privateroutes.stop.Stop;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Route")
@Table( name = "route" )
public class Route {

    public Route(Long id, String name, Date date) {
        this.id = id;
        this.name = name;
        this.date = date;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(
            name = "id",
            unique = true,
            nullable = false,
            updatable = false
    )
    private Long id;

    @Column(
            name = "name",
            nullable = false,
            updatable = false

    )
    private String name;

    @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    @Column(
            name = "date",
            nullable = false,
            updatable = false

    )
    private Date date;

    @OneToMany(
            mappedBy = "route",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @ToString.Exclude
    private List<Point> points;

    @OneToMany(
            mappedBy = "route",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @ToString.Exclude
    private List<Stop> stops;

    public void setPoints(List<Point> points) {
        this.points = points;
        for ( Point p : this.points ) {
            p.setRoute(this);
        }
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops;
        for ( Stop s : this.stops ) {
            s.setRoute(this);
        }
    }
}
