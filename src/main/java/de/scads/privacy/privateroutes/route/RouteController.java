package de.scads.privacy.privateroutes.route;

import de.scads.privacy.privateroutes.dto.DtoService;
import de.scads.privacy.privateroutes.dto.bidirectional.MeasurementDto;
import de.scads.privacy.privateroutes.dto.input.GenericRouteDto;
import de.scads.privacy.privateroutes.dto.output.*;
import de.scads.privacy.privateroutes.helper.ConversionHelper;
import de.scads.privacy.privateroutes.measurement.Measurement;
import de.scads.privacy.privateroutes.privacyService.PrivacyService;
import de.scads.privacy.privateroutes.privacyService.PrivacyServiceFlask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBufferLimitException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@RequestMapping("api/route")
@RestController
public class RouteController {

    private final RouteService routeService;

    private final DtoService dtoService;

    private final ConversionHelper conversionHelper;

    private final PrivacyService privacyService;

    @Autowired
    public RouteController(
            RouteService routeService,
            DtoService dtoService,
            ConversionHelper conversionHelper,
            PrivacyService privacyService
    ) {
        this.routeService = routeService;
        this.dtoService = dtoService;
        this.conversionHelper = conversionHelper;
        this.privacyService = privacyService;
    }

    @PostMapping(path = "add/generic")
    public boolean addRoute(@Valid @NotNull @NotEmpty @RequestBody GenericRouteDto routeDto) {
        // Convert Dto to Route
        Route route;
        try {
            route = conversionHelper.convertGenericRouteToRoute(routeDto);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }

        // Conversion threw no Exception, but route is still null? -> There is an unknown error, return false;
        if ( route == null ) {
            return false;
        }

        // Set measurements for point, if present
        if( routeDto.getMeasurements() != null ){
            if ( routeDto.getMeasurements().length != route.getPoints().size() ) {
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "The route contains " + routeDto.getMeasurements().length
                        + " measurements, but " + route.getPoints().size() + " coordinates. Numbers have to match! Discarding the route..");
            }
            for ( int i = 0; i < routeDto.getMeasurements().length; i++) {
                List<Measurement> measurementsForPoint = new ArrayList<>();
                for (MeasurementDto measurementDto : routeDto.getMeasurements()[i]) {
                    measurementsForPoint.add(dtoService.convertMeasurementDto(measurementDto));
                }
                route.getPoints().get(i).setMeasurements(measurementsForPoint);
            }
        }

        // Try to add the route to the db
        if (!routeService.addRoute(route)) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "The route already exists in the database");
        }

        // No errors, route was successfully added to db
        return true;
    }

    @GetMapping("")
    public List<RouteDto> getAllRoutes() {
        return routeService.getAllRoutes().stream()
                .map(dtoService::convertRoute)
                .collect(Collectors.toList());
    }


    @GetMapping(path = "{id}")
    private RouteDto getRouteById(@PathVariable("id") Long id) {
        return routeService.getRouteById(id)
                .map(dtoService::convertRoute)
                .orElse(null);
    }

    private List<RouteDto> getRoutesByIds(List<Long> ids) {
        List<RouteDto> routes = new ArrayList<>();
        for (Long id : ids) {
            routes.add(getRouteById(id));
        }
        return routes;
    }

    @DeleteMapping(path = "{id}")
    public void deleteRouteById(@PathVariable("id") Long id) {
        routeService.deleteRoute(id);
    }

    @GetMapping("publicAndPrivateRoutes")
    public PublicAndPrivateRoutesDto getPublicAndPrivateRoutes(@RequestParam List<Long> routeIds,
                                                               @RequestParam Long requestedPrivacyAlgorithm,
                                                               @RequestParam List<Double> parameterValue,
                                                               @RequestParam List<Long> metricIds) {
        List<RouteDto> selectedRoutes = getRoutesByIds(routeIds);
        return getCompleteRoutes(selectedRoutes, null, requestedPrivacyAlgorithm, parameterValue, metricIds, true);
    }

    @GetMapping("publicRoutes")
    public PublicAndPrivateRoutesDto getPublicRoutes(@RequestParam List<Long> routeIds,
                                                     @RequestParam List<Long> metricIds) {
        List<RouteDto> selectedRoutes = getRoutesByIds(routeIds);
        return getCompleteRoutes(selectedRoutes, null, null, null, metricIds, true);
    }

    @GetMapping("privateRoutes")
    public PublicAndPrivateRoutesDto getPrivateRoutes(@RequestParam List<Long> routeIds,
                                                      @RequestParam Long requestedPrivacyAlgorithm,
                                                      @RequestParam List<Double> parameterValue,
                                                      @RequestParam List<Long> metricIds) {
        List<RouteDto> selectedRoutes = getRoutesByIds(routeIds);
        return getCompleteRoutes(selectedRoutes, null, requestedPrivacyAlgorithm, parameterValue, metricIds, false);
    }

    /**
     * Makes different calls, depending on the input, to calculate all metrics or only the newly selected ones for the
     * private and/or the public routes.
     * @return PublicAndPrivateRoutesDto with all requested metrics
     */
    @PostMapping("changedMetrics")
    public PublicAndPrivateRoutesDto getRoutesWithChangedMetricsNew(@RequestParam List<Long> routeIds,
                                                                    @RequestParam List<Long> metricIds,
                                                                    @RequestBody PublicAndPrivateRoutesDto publicAndPrivateRoutes) {
        List<RouteDto> selectedRoutes = getRoutesByIds(routeIds);
        if (Objects.equals(
                publicAndPrivateRoutes.getNonPrivateRoutes().getRouteIds().stream().sorted().collect(Collectors.toList()),
                routeIds.stream().sorted().collect(Collectors.toList())
        )) {
            List<Long> newMetricIds = new ArrayList<>(metricIds);
            newMetricIds.removeAll(publicAndPrivateRoutes.getPrivateRoutes().getMetricIds());
            if (new HashSet<>(publicAndPrivateRoutes.getNonPrivateRoutes().getMetricIds()).containsAll(metricIds)
                    && publicAndPrivateRoutes.getPrivateRoutes().getRoutes().size() > 0) {
                PublicAndPrivateRoutesDto routesWithNewMetrics = getCompleteRoutes(selectedRoutes,
                        publicAndPrivateRoutes.getPrivateRoutes().getRoutes(), null,
                        null, newMetricIds, false);
                publicAndPrivateRoutes.getPrivateRoutes().addNewMetrics(routesWithNewMetrics.getPrivateRoutes());
            } else if (publicAndPrivateRoutes.getPrivateRoutes().getRoutes().size() > 0) {
                PublicAndPrivateRoutesDto routesWithNewMetrics = getCompleteRoutes(selectedRoutes,
                        publicAndPrivateRoutes.getPrivateRoutes().getRoutes(), null,
                        null, newMetricIds, true);
                publicAndPrivateRoutes.addNewMetrics(routesWithNewMetrics);
            } else {
                newMetricIds.removeAll(publicAndPrivateRoutes.getNonPrivateRoutes().getMetricIds());
                PublicAndPrivateRoutesDto routesWithNewMetrics = getCompleteRoutes(selectedRoutes,
                        publicAndPrivateRoutes.getPrivateRoutes().getRoutes(), null,
                        null, newMetricIds, true);
                publicAndPrivateRoutes.getNonPrivateRoutes().addNewMetrics(routesWithNewMetrics.getNonPrivateRoutes());
            }
            return publicAndPrivateRoutes;
        }
        return getCompleteRoutes(selectedRoutes, publicAndPrivateRoutes.getPrivateRoutes().getRoutes(),
                null, null, metricIds, true);
    }

    public PublicAndPrivateRoutesDto getCompleteRoutes(List<RouteDto> routes,
                                                       List<RouteWithMetricsDto> privateRoutes,
                                                       Long requestedPrivacyAlgorithm,
                                                       List<Double> parameterValue,
                                                       List<Long> metricIds,
                                                       boolean calculateNonPrivateMetrics
    ) {
        PublicAndPrivateRoutesDto completeRoutes = new PublicAndPrivateRoutesDto();
            PrivacyAlgorithmSettingsDto privacyAlgorithm = null;
            if (requestedPrivacyAlgorithm != null) {
                privacyAlgorithm = new PrivacyAlgorithmSettingsDto(requestedPrivacyAlgorithm, parameterValue);
            }
            try {
                return privacyService.getCompleteRoutes(new PrivacyServiceRequestDto(
                        routes,
                        privateRoutes,
                        metricIds,
                        calculateNonPrivateMetrics,
                        privacyAlgorithm
                ));
            }
        catch (DataBufferLimitException e) {
            System.out.println("Buffer limit exceeded");
        }
        catch (WebClientResponseException e) {
            System.out.println("Service response error");
        }
        return completeRoutes;
    }

    /**
     * Endpoint to get an overview of all the routes currently stored in the database.
     * @return a list of RouteOverview's. Each RouteOverview contains information about one route.
     */
    @GetMapping(path = "overview")
    public List<RouteOverview> getOverview(){
        return routeService.getOverview();
    }

    @DeleteMapping(path = "{id}/dropEvery/{everyNth}")
    public void dropEveryNthPoint(
            @PathVariable("id") Long routeId,
            @PathVariable("everyNth") int everyNth
    ){
        this.routeService.deleteEveryNthPoint( routeId, everyNth );
    }

}
