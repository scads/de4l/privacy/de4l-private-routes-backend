package de.scads.privacy.privateroutes.route;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface RouteRepository extends JpaRepository<Route, Long> {
    boolean existsByNameAndDate(String name, Date date);

    Optional<Route> findByNameAndDate(String name, Date date);

    @Query(value = " Select new de.scads.privacy.privateroutes.route.RouteOverview(r.id,r.name,r.date, count(p), CAST( st_length( st_transform( st_makeline( p.geography), 26986 ) ) as double ) / count(p) ) from Point p join p.route r group by r " )
    List<RouteOverview> getOverview();

    @Query(value = "Select distinct m.type from Point p join Measurement m on p = m.point where p.route.id = :routeId")
    List<String> getAvailableMeasurementsByRouteId(Long routeId);

}