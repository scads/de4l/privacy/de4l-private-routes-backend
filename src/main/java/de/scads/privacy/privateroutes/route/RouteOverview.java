package de.scads.privacy.privateroutes.route;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simplified representation of a route.
 * Used in the privacy-frontend to display an overview of all routes in the database.
 * The number of points in the route and the average step width are included to give the user more information
 * about the data
 */
@Data
public class RouteOverview {
    Long id;
    String name;
    Date date;
    Long pointCount;
    Double stepwidth;
    List<String> measurements;

    public RouteOverview(Long id, String name, Date date, Long pointCount, Double stepwidth) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.pointCount = pointCount;
        this.stepwidth = stepwidth;
        this.measurements = new ArrayList<>();
    }

}