package de.scads.privacy.privateroutes.privacyAlgorithm;

import de.scads.privacy.privateroutes.dto.bidirectional.PrivacyAlgorithmMetadataDto;
import de.scads.privacy.privateroutes.privacyService.PrivacyService;
import de.scads.privacy.privateroutes.privacyService.PrivacyServiceFlask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping("api/privacyAlgorithm")
@RestController
public class PrivacyAlgorithmController {

    private final PrivacyService privacyService;

    @Autowired
    public PrivacyAlgorithmController(
            PrivacyService privacyService
    ) {
        this.privacyService = privacyService;
    }

    @GetMapping("")
    public PrivacyAlgorithmMetadataDto[] getAvailableAlgorithms(){
        return privacyService.getAvailableAlgorithms();
    }
}
