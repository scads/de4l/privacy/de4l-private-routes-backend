package de.scads.privacy.privateroutes;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class De4lPrivateRoutesBackendApplication {


	public static void main(String[] args) {
		ConfigurableApplicationContext run = SpringApplication.run(De4lPrivateRoutesBackendApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
