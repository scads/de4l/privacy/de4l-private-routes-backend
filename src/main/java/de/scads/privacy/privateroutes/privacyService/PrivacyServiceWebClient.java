package de.scads.privacy.privateroutes.privacyService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class PrivacyServiceWebClient {

    @Value("${privacyServices.url}")
    private String baseUrl;

    @Bean
    public WebClient getWebClient(WebClient.Builder builder){
        return builder
                .clone()
                .baseUrl(baseUrl)
                .defaultHeaders( h -> h.setContentType(MediaType.APPLICATION_JSON) )
                .build();
    }
}

