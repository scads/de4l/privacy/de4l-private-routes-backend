package de.scads.privacy.privateroutes.privacyService;

import de.scads.privacy.privateroutes.dto.bidirectional.MetricMetadataDto;
import de.scads.privacy.privateroutes.dto.bidirectional.PrivacyAlgorithmMetadataDto;
import de.scads.privacy.privateroutes.dto.output.PrivacyServiceRequestDto;
import de.scads.privacy.privateroutes.dto.output.PublicAndPrivateRoutesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;


@Service
@ConditionalOnProperty(
        value="privacyServices.fake",
        havingValue = "false",
        matchIfMissing = true)
public class PrivacyServiceFlask implements PrivacyService{

    private final WebClient webClient;

    @Autowired
    public PrivacyServiceFlask(
            WebClient webClient
    ){
        this.webClient = webClient;
    }

    public PrivacyAlgorithmMetadataDto[] getAvailableAlgorithms(){
        Mono<PrivacyAlgorithmMetadataDto[]> response = webClient
                .get()
                .uri("privacyAlgorithm")
                .retrieve()
                .bodyToMono(PrivacyAlgorithmMetadataDto[].class);
        return response.block();
    }

    public MetricMetadataDto[] getAvailableMetrics() {
        Mono<MetricMetadataDto[]> response = webClient
                .get()
                .uri("metric")
                .retrieve()
                .bodyToMono(MetricMetadataDto[].class);
        return response.block();
    }

    public PublicAndPrivateRoutesDto getCompleteRoutes(
            PrivacyServiceRequestDto requestDto
    ) {
        Mono<PublicAndPrivateRoutesDto> response = webClient
                .post()
                .uri("route")
                .body(BodyInserters.fromValue(requestDto))
                .retrieve()
                .bodyToMono(PublicAndPrivateRoutesDto.class);
        return response.block();
    }
}
