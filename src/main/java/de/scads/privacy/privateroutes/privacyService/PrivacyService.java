package de.scads.privacy.privateroutes.privacyService;

import de.scads.privacy.privateroutes.dto.bidirectional.MetricMetadataDto;
import de.scads.privacy.privateroutes.dto.bidirectional.PrivacyAlgorithmMetadataDto;
import de.scads.privacy.privateroutes.dto.output.PrivacyServiceRequestDto;
import de.scads.privacy.privateroutes.dto.output.PublicAndPrivateRoutesDto;

public interface PrivacyService {

    public PublicAndPrivateRoutesDto getCompleteRoutes(PrivacyServiceRequestDto requestDto);

    public PrivacyAlgorithmMetadataDto[] getAvailableAlgorithms();

    public MetricMetadataDto[] getAvailableMetrics();
}
