package de.scads.privacy.privateroutes.settings;

import org.geolatte.geom.G2D;
import org.geolatte.geom.crs.CoordinateReferenceSystem;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.geolatte.geom.crs.CoordinateReferenceSystems.WGS84;

@Configuration
public class GeometrySettings {

    @Bean
    static CoordinateReferenceSystem<G2D> getCrs(){
        return WGS84;
    }


}
