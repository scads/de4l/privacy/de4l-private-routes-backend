package de.scads.privacy.privateroutes.settings;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Position;
import org.geolatte.geom.crs.CoordinateReferenceSystem;
import org.geolatte.geom.json.GeolatteGeomModule;
import org.geolatte.geom.json.Setting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.Transient;

import static org.geolatte.geom.crs.CoordinateReferenceSystems.WGS84;

@Configuration
public class JacksonSettings {

    @Autowired
    public void configureObjectMapper( ObjectMapper objectMapper, CoordinateReferenceSystem<G2D> crs) {

        // Configure Jackson to use Geolatte's serializer for Geodata
        GeolatteGeomModule geoModule = new GeolatteGeomModule(crs);
        // Don't put the reference coordinate system in the serialized json
        geoModule.set(Setting.SUPPRESS_CRS_SERIALIZATION, true);
        objectMapper.registerModule(geoModule);
    }
}
