package de.scads.privacy.privateroutes.stop;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.scads.privacy.privateroutes.route.Route;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.geolatte.geom.G2D;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "privacy_tuna_stop")
public class Stop implements Serializable {

    public Stop(org.geolatte.geom.Point<G2D> g2DPoint, Date date) {
        this.geography = g2DPoint;
        this.timestamp = date;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(
            name = "id",
            updatable = false
    )
    Long id;

    @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    @Column(
            name = "timestamp",
            nullable = false
    )
    private Date timestamp;

    @ManyToOne( fetch = FetchType.LAZY )
    private Route route;

    private org.geolatte.geom.Point<G2D> geography;

    @JsonIgnore
    public Double getLatitude(){
        return geography.getPosition().getLat();
    }

    @JsonIgnore
    public Double getLongitude(){
        return geography.getPosition().getLon();
    }
}
