package de.scads.privacy.privateroutes.stop;

import org.geolatte.geom.G2D;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface StopRepository extends JpaRepository<Stop, Long> {

    boolean existsByGeographyAndTimestamp(org.geolatte.geom.Point<G2D> geography, Date timestamp);

}
