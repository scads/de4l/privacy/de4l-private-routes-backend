package de.scads.privacy.privateroutes.stop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StopService {

    private final StopRepository stopRepository;

    @Autowired
    public StopService(StopRepository stopRepository) {
        this.stopRepository = stopRepository;
    }

    public boolean addStop(Stop stop) {
        if (!stopRepository.existsByGeographyAndTimestamp(stop.getGeography(), stop.getTimestamp())) {
            stopRepository.save(stop);
            return true;
        } else {
            return false;
        }
    }

}
