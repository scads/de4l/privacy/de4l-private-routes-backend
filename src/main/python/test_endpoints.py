import requests
import pandas
import json


def post_data(data_frame, endpoint, headers=None):
    for idx, row in data_frame.iterrows():
        el = json.loads(row.to_json(date_format="iso"))
        requests.post(endpoint, json=el, headers=headers)
        print(el)


def get_data_from_all_tables(headers=None):
    for datatype in ['measurement', 'point', 'route']:
        r = requests.get('http://localhost:8080/api/' + datatype, headers=headers)
        print(r, r.text)


def get_private_points_via_each_privacy_service(headers=None):
    for service in ['noise2d', 'promesse']:
        r = requests.get('http://localhost:8080/api/point/privacy/' + service + '?parameterValue=1', headers=headers)
        print(r, r.text)


def get_keycloak_authorization_token():
    auth_server_url = 'https://auth.de4l.io/auth/'
    realm_name = 'de4l'
    privacy_client = 'de4l-privacy-service'
    user = 'testuser'
    password = 'Password123'

    data = {'grant_type': 'password', 'client_id': privacy_client, 'username': user, 'password': password}
    url = auth_server_url + "realms/" + realm_name + "/protocol/openid-connect/token"
    response = requests.post(url=url, data=data)
    token = json.loads(response.text)
    headers = {'Authorization': 'Bearer %s' % token['access_token']}
    return headers


# setup
data_frame_de4l = pandas.read_json("../resources/test-sensor-dataset-small.json", lines=True)
data_frame_de4l_with_stop = pandas.read_json("../resources/test-sensor-dataset-small-2.json", lines=True)
data_frame_de4l_with_stop["isStop"] = False
data_frame_de4l_with_stop.at[0, "isStop"] = True

data_frame_taxi = pandas.read_csv("../resources/first20.csv", sep=",", encoding="latin1")
data_frame_taxi_with_stop = pandas.read_csv("../resources/test-taxi-dataset-small-2.csv", sep=",", encoding="latin1")
data_frame_taxi_with_stop["stops"] = "[[]]"
data_frame_taxi_with_stop.at[0, "stops"] = "[[-8.574306167488116, 41.166529844118756], [-8.585739, 41.148558], [-8.5743494432479, 41.1676585023774]]"
generic_route = json.loads('{"name": "utcname","route": "[[-8.61, 41.16]]","stops": "[[-8.61, 41.16]]","timestamp": "2021-12-03T10:37:02.000Z"}')

endpoint_measurement = 'http://localhost:8080/api/measurement/add/de4l'
endpoint_route_taxi = 'http://localhost:8080/api/route/add/taxi'
endpoint_route_generic = 'http://localhost:8080/api/route/add/generic'


# post and get de4l and taxi data
# non-authorized and authorized requests (non-authorized requests have no effect)
for authorization_headers in [None, get_keycloak_authorization_token()]:
    # post_data(data_frame_de4l, endpoint_measurement, authorization_headers)
    # post_data(data_frame_de4l_with_stop, endpoint_measurement, authorization_headers)
    post_data(data_frame_taxi, endpoint_route_taxi, authorization_headers)
    # post_data(data_frame_taxi_with_stop, endpoint_route_taxi, authorization_headers)
    # requests.post(endpoint_route_generic, json=generic_route, headers=authorization_headers)
    get_data_from_all_tables(authorization_headers)
    # get_private_points_via_each_privacy_service(authorization_headers)
