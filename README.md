# Privacy Tuna SpringBoot Backend

SpringBoot Backend for the Privacy Tuna application, managing the database and the data flow between all components.

## Development

Follow the following steps to start developing:

1. Clone this repository via git and change into the directory.
    ```bash
    git clone git+https://git@git.informatik.uni-leipzig.de:scads/de4l/privacy/de4l-private-routes-backend.git
    cd de4l-private-routes-backend
    ```
2. We use a [PostGIS](https://postgis.net/) enabled PostgreSQL database to store the data. The database can be started via:
   ```bash
	 docker-compose up -d
   ```
   To stop the database, run:
   ```bash
	 docker-compose down
   ```

3. The project is managed with gradle. If you don't have it installed visit [gradle.org](https://gradle.org/install/) for installation instructions. 
   Run the application with:
   ```
   ./gradlew bootRun
   ```
   To continue the development of the *Privacy Tuna* application, also check the [frontend](https://git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-private-routes-frontend) and the second [backend](https://git.informatik.uni-leipzig.de/scads/de4l/privacy/de4l-privacy-services).

## Build
To build a new container, first create a new jar with: 
```bash
./gradlew clean build -x test
```

After the jar is successfully created, build the container with:
```bash
docker build -t private-routes-backend:latest .
```
The container will use the spring.profile=production. In this profile the credentials, host and port of the database are read from environment variables. 
(Check the application-production.yml for details.) 
